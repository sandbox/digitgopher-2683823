
README file for the Node Clone Relation module for Drupal 7.x.

The Node Clone Relation module saves the relationship between a cloned node and
the node it was cloned from. Since 'parent' data is saved, this creates a linked
hierarchy. Each node can only have one parent, but many children. If node A is
cloned to node B, and then node B is cloned to node C, and then node B is
deleted: node C now appears to be cloned from node A.

Because cloned information cannot be saved retroactively, all clone relations
will be saved only from the time this module is enabled. If you have an existing
site, it will have to be bootstrapped in!

There is support for:
- views
- context

Because it is a hierarchy, it is possible to use the views_tree module. In the
view, add the relationship to parent node, then set the Tree formatter 'Main
field' to nid and 'Parent field' to Parent nid.

To install this module, copy the folder with all the files to the
/sites/all/modules  OR /sites/default/modules directory of your Drupal
installation and enable it at /admin/build/modules. A new database table will be
created (node_clone), and no configurations are needed.

Note: this module originally derived from code posted by Sophie Shanahan-Kluth
(sophiesk@drupal) at https://www.drupal.org/node/2466867
