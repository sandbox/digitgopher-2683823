<?php
/**
 * @file
 * Views field handler for Node Clone Relation module.
 */

/**
 * Field handler to create a link to the parent node.
 */
class views_handler_field_node_clone_relation_pnid extends views_handler_field {
  /**
   * Renders the link.
   */
  function render($values) {
    if (isset($values->node_clone_pnid) && is_numeric($values->node_clone_pnid)) {
      // Retrieve the parent node.
      $parent_node = node_load($values->node_clone_pnid);

      if (is_object($parent_node) && node_access('view', $parent_node)) {
        $path = drupal_get_path_alias('node/' . $parent_node->nid);
        $link = l($parent_node->title, $path);
        return $link;
      }
      else {
        return $parent_node->title;
      }
    }

    // If we don't have anything just render as parent.
    return parent::render($values);
  }
}
