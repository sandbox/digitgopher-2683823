<?php
/**
 * @file
 * Views settings for Node Clone Relation module.
 */

/**
 * Implements hook_views_data().
 */
function clone_views_data() {
  $data = array();

  // Define our new 'node clone' group.
  $data['node_clone']['table']['group'] = t('Node clone');

  // Join it to the 'node' table.
  $data['node_clone']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
  );

  // Define our fields.
  $data['node_clone']['pnid'] = array(
    'title' => t('Node is cloned'),
    'field' => array(
      'title' => t('Parent node'),
      'help' => t('Provide a link to the parent node.'),
      'click sortable' => TRUE,
      'handler' => 'views_handler_field_node_clone_relation_pnid',
    ),
    'filter' => array(
      'label' => t('Node is a clone'),
      'help' => t('Filter by cloned status of nodes.'),
      'handler' => 'views_handler_filter_boolean_operator',
      'accept null' => TRUE,
    ),
    'argument' => array(
      'label' => t('Parent node'),
      'help' => t('Filter by parent node ID.'),
      'handler' => 'views_handler_argument',
    ),
    'relationship' => array(
      'base' => 'node',
      'base field' => 'nid',
      'handler' => 'views_handler_relationship',
      'label' => t('Parent of cloned node'),
      'title' => t('Cloned from'),
      'help' => t('A bridge to the parent of the cloned node'),
    ),
  );

  return $data;
}
