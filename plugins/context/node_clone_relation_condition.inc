<?php
/**
 * @file
 * Provides node clone context extension.
 */

/**
 * Expose node clone properties as a Context condition.
 */
class node_clone_relation_condition extends context_condition {
  function condition_values() {
    return array(
      'is_clone' => t('Node is a clone'),
      'has_clone' => t('Node has a clone'),
      'either' => t('Node is a clone OR has a clone'),
      'both' => t('Node is a clone AND has a clone'),
    );
  }

  function condition_form($context) {
    $form = parent::condition_form($context);

    $form['#type'] = 'radios';
    if(empty($form['#default_value'])){
      $form['#default_value'] = 'is_clone';
    }
    else{
      $form['#default_value'] = current($form['#default_value']);
    }
    return $form;
  }

  /**
   * Condition form submit handler.
   */
  function condition_form_submit($values) {
    return array_filter(array($values => $values));
  }

  function execute($node) {
    foreach ($this->get_contexts() as $context) {
      $values = $this->fetch_from_context($context, 'values');
      $nid = $node->nid;
      $is_child = retrieve_parent_node_info($nid);
      $has_child = clone_is_parent($nid);

      if (!empty($values['is_clone'])) {
        if ($is_child) {
          $this->condition_met($context);
        }
      }
      elseif (!empty($values['has_clone'])) {
        if ($has_child) {
          $this->condition_met($context);
        }
      }
      elseif (!empty($values['either'])) {
        if ($is_child || $has_child) {
          $this->condition_met($context);
        }
      }
      elseif (!empty($values['both'])) {
        if ($is_child && $has_child) {
          $this->condition_met($context);
        }
      }
    }
  }
}
